<style>
code{color:#039BE5;font-weight:bold;background-color:#ddd;padding:1px 10px 2px 10px;border-radius:10px;display:inline-block;}
pre{overflow-x: auto;}
img{width: 100%;height: auto;}
</style>
# Install k3s

To install <a href="https://k3s.io" target=_blank>k3s</a> we only need to start the installer script:

```ctr:node1
curl -sfL https://get.k3s.io | sh -s - --docker
```

To verrify our installation we can run our first `kubectl` commands:
```ctr:node1
kubectl get nodes
```

Repeat the `kubectl get nodes` command until your node is Ready:

```
NAME                        STATUS   ROLES                  AGE   VERSION
dynamic-d611d165-8781479c   Ready    control-plane,master   12s   v1.20.6+k3s1
```